# Shell Customization

## Step 1: Preparing your shell

Add to your ~/.zshrc or ~/.bash_profile file

```zsh
############ SHELL CUSTOMIZATION ##############
# Path to directory you store your shell customization in
export SHELL_CUSTOMIZATION_DIR="${HOME}/.shell-customization"
source ${SHELL_CUSTOMIZATION_DIR}/default
```

Exit and close your shell, then restart it. You will get a notice that the ${SHELL_CUSTOMIZATION_DIR}/default file doesn't exist, that is because we haven't created it yet. The next step will create that file with the git clone.

## Step 2: Adding shell customiztion repo

Run from command line

```zsh
git clone git@bitbucket.org:JoshuaMartinez/shell-sync.git ${SHELL_CUSTOMIZATION_DIR}
```

Exit and close your shell, then restart it. You should now have the customizations in your shell.

## Other

To add more customizations to this project either edit one of the files in the extension directory or create a new file and put it in the extension directory. When the default file is loaded on shell launch it will autoload any files in the extension directory also.

Q: zsh already has a mechanisim for customizations, so why do it this way?

A: The way this is configured you can use it for either bash or zsh, you are not limited to just one and you can keep it in github so it is portable.
